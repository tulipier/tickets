#!/usr/bin/python

import sys, os
from subprocess import call
import glob
import argparse

from printer import UsbEPSON_TM_T88V

doConvert = False

parser = argparse.ArgumentParser()
parser.add_argument("print", type=str)
parser.add_argument("-f", "--folder", action="store_true")
parser.add_argument("-i", "--image", action="store_true")
parser.add_argument("-t", "--text", action="store_true")
parser.add_argument("-r", "--remove", action="store_true")
parser.add_argument("-c", "--cut", action="store_true")
parser.add_argument("-s", "--sort", action="store_true")
parser.add_argument("-n", "--number", type=int, default=1)
args = parser.parse_args()

printer = UsbEPSON_TM_T88V()


for i in range(args.number):
    if args.text:
        printer.text(args.print)
        if args.cut:
            printer.cut()
    elif args.folder:
        files = glob.glob(os.path.join(args.print, '*'))
        if args.sort:
            files = sorted(files)
        for f in files:
            printer.image(f)
            if args.remove:
                os.remove(f)
            if args.cut:
                printer.cut()
    elif args.image:
        printer.image(args.print)
        if args.cut:
            printer.cut()






