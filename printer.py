import escpos.printer


class UsbEPSON_TM_T88V(escpos.printer.Usb):
    def __init__(self):
        super(UsbEPSON_TM_T88V, self).__init__(0x04b8, 0x0202)